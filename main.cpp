#include <iostream>
#include <cmath>
#include <string>
#include <sstream>
#include <fstream>

using namespace std;

const int N = 20000;

void InputMatrix(int DubKol, int  matrix[N]) // ввод последовательности
{
    cout<<"Введите саму последовательность: ";
    for(int x = 0 ; x < DubKol ; x += 2)
        cin>>matrix[x];
}

int SimpleNum(int a) // определение простого числа
{
    for(int i=2;i <= a/2;i++)
        if( (a % i) == 0 )
            return 0;
    return 1;
}

void DeletEasynOutputMatrix(int DubKol , int mas[N] ) // Удаление простого числа
{
    int KolVoEasy = 0;
    for (int i = 0; i < DubKol; i+=2)
    {
        if ((SimpleNum(mas[i])) && (mas[i] != 1))
        {
            mas[i] = 0;
            KolVoEasy ++;
        }
    }

    int temp;

    for (int i = 0; i < DubKol - 2; i+=2)
        for(int j = i + 2; j < DubKol ; j +=2)
            if (mas[i] == 0)
            {
                temp = mas[i];
                mas[i] = mas[j];
                mas[j]=mas[i+2];
                mas[i+2]=temp;
            }

    int Chisl;

    Chisl = DubKol - 2*KolVoEasy;

    int SumOfCif = 0;

    int ChislvMas;


    for(int i = 0 ; i < Chisl ; i+=2)// сумма цифр числа
    {
        ChislvMas = mas[i];
        while (ChislvMas != 0)
        {
            SumOfCif += ChislvMas % 10;
            ChislvMas = ChislvMas / 10;
        }
        if(SumOfCif == 15)
        {
            mas[i+1]=mas[i];
            cout<<mas[i]<<" "<<mas[i+1]<<" ";
        }
        else
            cout<<mas[i]<<" ";

        SumOfCif = 0;
    }
}


int main()
{
    int KolVo;
    cout<<"Введите количество чисел в последовательности: ";
    cin >> KolVo;
    int DubKol;
    DubKol = 2 * KolVo;
    int mas[DubKol];
    InputMatrix( DubKol, mas);
    DeletEasynOutputMatrix( DubKol , mas);

    return 0;
}

